
module.exports.ccdr = function(ccdr){
    var ccdrArray = new Array(5);
    var ccdrString = ccdr.replace(/[^0-9\.]+/g, "");
    if(ccdrString.length == 14){
        ccdrArray[0] = ccdrString.substring(0, 2);
        ccdrArray[1] = ccdrString.substring(2, 4);
        ccdrArray[2] = ccdrString.substring(4, 7);
        ccdrArray[3] = ccdrString.substring(7, 10);
        ccdrArray[4] = ccdrString.substring(10, 14);
        return ccdrArray;
    }else{
        throw "Invalid ccdr";
    }
}
module.exports.randomWait = function randomWait(min, max){
    if (max>min){
        return this.wait(Math.floor((Math.random() * max) + min));
    }else{
        throw "Invalid time arguments"
    }

}
module.exports.wait = function wait(number){
    if (number === null){
        number = 3500;
    }
    setTimeout(function(){
        return;
    }, null);
}

//


module.exports.elementWaiter = function(elementId){
    setInterval()
    if(this.elementChecker(elementId) === true){
        return true;
    }
}
module.exports.iterator =function(array, callback, scope){
    for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]); // passes back stuff we need
    }
}
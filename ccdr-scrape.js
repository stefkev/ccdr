var casper = require('casper').create({
    verbose: true
});
var x = require('casper').selectXPath();
var casperUtils = require("utils");
var util = require("./lib/util");


var url = "http://12.218.239.81/i2/default.aspx";

var firstbox = "#SearchFormEx1_PINTextBox0";
var secondbox = "#SearchFormEx1_PINTextBox1";
var thirdbox = "#SearchFormEx1_PINTextBox2";
var fourthbox = "#SearchFormEx1_PINTextBox3";
var fifthbox = "#SearchFormEx1_PINTextBox4";
var boxButtonSearch = "#SearchFormEx1_btnSearch";
var resaultContainerLeft = "#DocList1_ContentContainer1";
var clickableLinksLeft = [];


//casperjs ccdr-scrape.js 15-05-224-017-0000 --proxy=davinchi:lk3409jpweo0-c@192.255.96.77:80 --web-security=no --ignore-ssl-errors=yes
var filteredCcdr = util.ccdr(casper.cli.args[0]);

    casper.start(url).viewport(1600, 1000);
    casper.then(function(){
        this.sendKeys(firstbox, filteredCcdr[0]);
        this.sendKeys(secondbox, filteredCcdr[1]);
        this.sendKeys(thirdbox, filteredCcdr[2]);
        this.sendKeys(fourthbox, filteredCcdr[3]);
        this.sendKeys(fifthbox, filteredCcdr[4]);
        this.click(boxButtonSearch);
    })
    casper.then(function(){
        this.waitForSelector(resaultContainerLeft)
    })
    casper.then(function(){
        clickableLinksLeft = [];
        var tr_data = this.evaluate(function() {
            var nodes = document.querySelectorAll('#DocList1_ContentContainer1 > table > tbody > tr:nth-child(1) > td > div > div:nth-child(2) > table > tbody > tr ');
            return Array.prototype.map.call(nodes, function(tr, i) {
                return {
                    recordedDate: tr.children[1].innerText.trim(),
                    pin: tr.children[2].innerText.trim(),
                    typeDesc: tr.children[3].innerText.trim(),
                    doc: tr.children[4].innerText.trim(),
                    firstGrantor: tr.children[5].innerText.trim(),
                    firstGrantee: tr.children[6].innerText.trim(),
                    firstPriorDoc: tr.children[7].innerText.trim()
                }
            })
        });
        var links = this.evaluate(function(){
            var linknodes = document.querySelectorAll('#DocList1_ContentContainer1 > table > tbody > tr:nth-child(1) > td > div > div:nth-child(2) > table > tbody > tr > td > a');
            return Array.prototype.map.call(linknodes, function(a, i){
                return a.getAttribute('id')
            })
        })
        for (i=1; i<links.length;i=i+7){
            clickableLinksLeft.push(links[i]);
        }
        //this.echo(JSON.stringify(tr_data))
    })

casper.then(function() {
    var clickflag = 0;
    var i = 0;
    var innerHtml = [];
    casper.each(clickableLinksLeft, function(self, link){
        self.then(function(){
            console.log(link)
            this.click('#'+link);
        })
        self.then(function(){
            this.wait(3500);
        });
        self.then(function(){
            this.waitForSelector('#DocDetails1_Details_Table');
        })
        self.then(function(){
            docDetails_table = this.evaluate(function(){
                var selector = '#DocDetails1_GridView_Details > tbody > tr:nth-child(2)';
                var nodes = document.querySelectorAll(selector);
                return Array.prototype.map.call(nodes, function(node, i){
                    return {
                        documentNo : node.children[0].innerText,
                        executed : node.children[1].innerText,
                        recorded: node.children[2].innerText,
                        documentType: node.children[3].innerText,
                        caseNo: node.children[4].innerText,
                        amount: node.children[5].innerText
                    }
                })
            });
        });
        self.then(function() {
            legalDescriptionTable = this.evaluate(function(){
                var selector = '#DocDetails1_GridView_LegalDescription > tbody > tr:nth-child(2)';
                var nodes = document.querySelectorAll(selector);
                return Array.prototype.map.call(nodes, function(node, i){
                    return {
                        pin : node.children[0].innerText,
                        propType : node.children[1].innerText,
                        condoUnitNumber: node.children[2].innerText,
                        str: node.children[3].innerText,
                        subDivCondo: node.children[4].innerText,
                        lot: node.children[5].innerText,
                        block: node.children[6].innerText,
                        partOfLot: node.children[7].innerText
                    }
                })
            });
        })
        self.then(function(){
            var grantor_purified = [];
            grantor_table = [];
            grantor_table = this.evaluate(function(){
                var selector = '#DocDetails1_GridView_Grantor > tbody > tr > td > a';
                var nodes = document.querySelectorAll(selector);
                return Array.prototype.map.call(nodes, function(node, i){
                    return node.innerText
                })
            })
            for(i=0;i<grantor_table.length; i++){
                if(i%2==0){
                    var grantor = grantor_table[i];
                }else if (i%2!=0){
                    var trust = grantor_table[i];
                    var utilArray = [];
                    utilArray.push({grantor: grantor, trust: trust})
                    grantor_purified.push(utilArray)
                }
            }
            this.echo(JSON.stringify(grantor_purified))
        })
        self.then(function(){
            var grantee_purified = [];
            grantee_table = this.evaluate(function(){
                var selector = '#DocDetails1_GridView_Grantee > tbody > tr > td > a';
                var nodes = document.querySelectorAll(selector);
                return Array.prototype.map.call(nodes, function(node, i){
                    return node.innerText
                })
            })
            for (i=0; i<grantee_table.length; i++){
                if(i%2==0){
                    var grantee = grantee_table[i];
                }else if (i%2!=0){
                    var trust = grantee_table[i];
                    var utilArray = [];
                    utilArray.push({grantee: grantee, trust: trust})
                    grantee_purified.push(utilArray)
                }
            }
        })
        self.then(function(){
            var priorDocPurified = [];
            priorDoc_table = this.evaluate(function(){
                var selector = '#DocDetails1_GridView_Referance > tbody > tr > td';
                var nodes = document.querySelectorAll(selector);
                return Array.prototype.map.call(nodes, function(node, i){
                    return node.innerText;
                })
            })
            for (i=0; i<priorDoc_table.length; i++){
                if(i%2==0){
                    var doc = priorDoc_table[i];
                }else if (i%2!=0){
                    var year = priorDoc_table[i];
                    var utilArray = [];
                    utilArray.push({doc: doc, year: year})
                    priorDocPurified.push(utilArray)
                }
            }
            this.echo(JSON.stringify(priorDocPurified));
        })
        self.then(function(){
            i++
        })


        });

})


    casper.run();

////casperjs ccdr-scrape.js 15-05-224-017-0000 --proxy=davinchi:lk3409jpweo0-c@192.255.96.77:80 --web-security=no --ignore-ssl-errors=yes

